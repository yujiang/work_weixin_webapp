<!DOCTYPE html>
<html ng-app="workApp">
<head lang="en">
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="msapplication-tap-highlight" content="no" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width" />
    <meta http-equiv="Content-Security-Policy" content="default-src * 'unsafe-inline'; style-src 'self' 'unsafe-inline'; media-src *" />
    <link rel="stylesheet" type="text/css" href="css/ionic.css" />
    <link rel="stylesheet" type="text/css" href="css/workWeiXin.css" />
    <script type="text/javascript" src="js/zepto.min.js"></script>
    <script type="text/javascript" src="js/ionic.bundle.js"></script>
    <script type="text/javascript" src="node_modules/ionic-datepicker/dist/ionic-datepicker.bundle.min.js"></script>
    <script type="text/javascript" src="js/workApp.js"></script>
    <script type="text/javascript" src="js/workService.js"></script>
    <title>work weixin</title>
</head>

<body ng-controller="workCtrl">
    <div ui-view>
    </div>
    <ion-tabs class="tabs-icon-top tabs-color-positive">
        <ion-tab icon="ion-ios-list-outline" on-select="jump('home')" title="会议列表"></ion-tab>
        <ion-tab icon="ion-qr-scanner" ng-click="openCamera()" title="扫码"></ion-tab>
        <ion-tab icon="ion-ios-plus-outline" on-select="jump('order')" title="预约会议"></ion-tab>
    </ion-tabs>
</body>

<?php
    error_reporting(E_ALL || ~E_NOTICE);
    require_once "data/OpenApiSample/lib/helper.php";
    require_once "data/OpenApiSample/lib/jssdk.php";

    $jssdk=new JSSDK(1000004);
    $signPackage=$jssdk->GetSignPackage()
?>

<script type="text/javascript" src="js/jweixin-1.2.0.js"></script>
<script>
    jWeixin.config({
        debug: true,
        appId: '<?php echo $signPackage["appId"];?>',
        timestamp: <?php echo $signPackage["timestamp"];?>,
        nonceStr: '<?php echo $signPackage["nonceStr"];?>',
        signature: '<?php echo $signPackage["signature"];?>',
        jsApiList: [
            'checkJsApi',
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
            'onMenuShareWeibo',
            'onMenuShareQZone',
            'hideMenuItems',
            'showMenuItems',
            'hideAllNonBaseMenuItem',
            'showAllNonBaseMenuItem',
            'translateVoice',
            'startRecord',
            'stopRecord',
            'onVoiceRecordEnd',
            'playVoice',
            'onVoicePlayEnd',
            'pauseVoice',
            'stopVoice',
            'uploadVoice',
            'downloadVoice',
            'chooseImage',
            'previewImage',
            'uploadImage',
            'downloadImage',
            'getNetworkType',
            'openLocation',
            'getLocation',
            'hideOptionMenu',
            'showOptionMenu',
            'closeWindow',
            'scanQRCode',
            'chooseWXPay',
            'openProductSpecificView',
            'addCard',
            'chooseCard',
            'openCard'
        ]
    });
</script>
<script type="text/javascript" src="js/workController.js"></script>
<script type="text/javascript" src="js/homeController.js"></script>
<script type="text/javascript" src="js/orderController.js"></script>
</html>