
/**
 * Created by king-bc on 2017/5/11.
 */
angular.module('workApp').factory('WorkService',function ($http,$ionicLoading) {
    var workService={};

    //get请求服务
    workService.getHttp=function (url, handleSuc,handleErr) {
        $ionicLoading.show({template:'loading...'});
        $http.get(url).success(function (succ) {
            $ionicLoading.hide();
            handleSuc(succ);
        }).error(function (err) {
            $ionicLoading.hide();
            handleErr(err);
        })
    };

    //post请求服务
    workService.postHttp=function (url, params, handleSuc, handleErr) {
        $ionicLoading.show({template:'loading...'});
        $http.post(url,params).success(function (succ) {
            $ionicLoading.hide();
            handleSuc(succ);
        }).error(function (err) {
            $ionicLoading.hide();
            handleErr(err);
        })
    }

    return workService;
})