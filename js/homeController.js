angular.module('workApp').controller('homeCtrl',['$scope','WorkService','$ionicModal','$ionicPopup',function ($scope,WorkService,$ionicModal,$ionicPopup) {
    $scope.departmentList=[];
    $scope.titleList=[ 'Lorem', 'ipsum', 'dolor', 'sit', 'amet', 'consectetur', 'adipisicing'];
    //获取会议列表
    $scope.listFun=function () {
        for(var i=0;i<7;i++){
            var status='';
            switch (x=Math.floor(Math.random()*3)){
                case 0:status='positive'; break;
                case 1:status='balanced';break;
                case 2:status='assertive';break;
            }
            $scope.departmentList.push({
                id:i,
                title:$scope.titleList[i],
                room:`room${1}`,
                time:`05月${16+1}日`,
                status:status
            })
        }
    };
    $scope.listFun();



    // 当前日期
    $scope.meetingListDate=new Date().toLocaleDateString();
    // 上周,下周
    $scope.changeWeeks=function (i) {
        $scope.meetingListDate=new Date(new Date($scope.meetingListDate).getTime()+(i*86400000)).toLocaleDateString();
        $scope.departmentList=[];
        $scope.listFun();
    };

    // 点击事件
    $scope.showModal=function (status) {
        switch (status){
            case "positive":$scope.fun1();break;
            case "balanced":$scope.fun2();break;
            case "assertive":$scope.fun3();break;
        }
    };

    //蓝色，会议前不可签到，可以扫码签到、修改、取消
    $scope.fun1=function () {
        $scope.alert1=$ionicPopup.alert({
            template:`
                <button class="button button-block button-outline button-positive" ng-click="cancelFun(1)">取消会议</button>
                <button class="button button-block button-outline button-positive" ng-click="amendFun()">修改会议</button>
                <button class="button button-block  button-assertive" ng-click="alert1.close()">取消</button>
                `,
            scope:$scope,
            buttons:[]
        })
    };

    // 绿色，会议前可预约，可以扫码签到、取消
    $scope.fun2=function () {
        $scope.alert2=$ionicPopup.alert({
            template:`
                <button class="button button-block button-outline button-positive" ng-click="signFun()">扫码签到</button>
                <button class="button button-block button-outline button-positive" ng-click="cancelFun(2)">取消会议</button>
                <button class="button button-block  button-assertive" ng-click="alert2.close()">取消</button>
                `,
            scope:$scope,
            buttons:[]
        });
    };

    //红色，会议进行时，会议结束，延长
    $scope.fun3=function () {
        $scope.alert3=$ionicPopup.alert({
            template:`
                <button class="button button-block button-outline button-positive" ng-click="stopFun()">结束会议</button>
                <button class="button button-block button-outline button-positive" ng-click="addTime()">延长会议</button>
                <button class="button button-block  button-assertive" ng-click="alert3.close()">取消</button>
                `,
            scope:$scope,
            buttons:[]
        })
    };

    // 扫码签到
    $scope.signFun=function () {
        $scope.alert2.close();
        $ionicPopup.alert({
            title:'调用企业微信扫码接口'
        }).then(function () {

        })
    };

    //结束会议
    $scope.stopFun=function () {
        $scope.alert3.close();
        $ionicPopup.alert({
            title:'取消会议操作'
        }).then(function () {

        })

    };

    //延长会议
    $scope.addTime=function () {
        $scope.alert3.close();
        $ionicPopup.alert({
            title:'延长会议时长'
        }).then(function () {

        })
    };

    //修改会议
    $scope.amendFun=function () {
        $scope.alert1.close();
        $ionicPopup.alert({
            title:'修改会议内容'
        }).then(function () {

        })
    };

    //取消会议
    $scope.cancelFun=function (n) {
        switch (n){
            case 1:$scope.alert1.close();break;
            case 2:$scope.alert2.close();break;
        }
        $ionicPopup.alert({
            title:'取消会议'
        }).then(function () {

        })
    }
}]);