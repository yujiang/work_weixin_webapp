/**
 * Created by king-bc on 2017/5/18.
 */
angular.module('workApp').controller('orderCtrl',['$scope','WorkService','$ionicModal','$ionicPopup','ionicDatePicker',function ($scope,WorkService,$ionicModal,$ionicPopup,ionicDatePicker) {
    // 用户名
    $scope.orderUserName=localStorage.getItem('userName');
    // 会议主题
    $scope.meetingTitle=`${$scope.orderUserName}召开会议`;
    // 循环模式
    $scope.circulationMsg='不循环';

    // 会议日期
    $scope.date=new Date();
    $scope.meetingDate=`${$scope.date.getFullYear()}-${$scope.date.getMonth()+1}-${$scope.date.getDate()}`;

    $scope.openDatePicker=function () {
        var ipObj1 = {
            callback: function (val) {  //Mandatory
                $scope.meetingDate=(new Date(val)).toLocaleDateString();
            },
        };
        ionicDatePicker.openDatePicker(ipObj1);
    };


    // 会议时间
    $scope.meetingTime1={'volume':$scope.date.getHours()*2};
    $scope.meetingTime2={'volume':$scope.date.getHours()*2+2};
    // 起始时间监听
    $scope.$watch('meetingTime1.volume',function () {
        $scope.watchTime();
    });
    // 结束时间监听
    $scope.$watch('meetingTime2.volume',function () {
        $scope.watchTime();
    });
    // 时间监听函数
    $scope.watchTime=function () {
        ($scope.meetingTime1.volume%2==0)?$scope.tim1=`${parseInt($scope.meetingTime1.volume/2)}:00`:$scope.tim1=`${parseInt($scope.meetingTime1.volume/2)}:30`;
        ($scope.meetingTime2.volume%2==0)?$scope.tim2=`${parseInt($scope.meetingTime2.volume/2)}:00`:$scope.tim2=`${parseInt($scope.meetingTime2.volume/2)}:30`;
        $scope.meetingTime=`${$scope.tim1}至${$scope.tim2}`;
    };

    //回忆循环模式选择
    $scope.circulation=function () {
        $scope.showAlert=$ionicPopup.alert({
            template:`<button class="button button-block button-outline button-balanced" ng-click="closeCirculation('不循环')">不循环</button><button class="button button-block button-outline button-balanced" ng-click="closeCirculation('每天')">每天</button><button class="button button-block button-outline button-balanced" ng-click="closeCirculation('每周')">每周</button><button class="button button-block button-outline button-balanced" ng-click="closeCirculation('每月')">每月</button>`,
            title:'选择循环模式',
            scope:$scope,
            buttons:[]
        })
    };
    // 循环模式选择后关闭alert，更新数据
    $scope.closeCirculation=function (msg) {
        $scope.showAlert.close();
        $scope.circulationMsg=msg;
    };

    // 会议类型
    $scope.meetingType='普通';
    $scope.meetingTypeShow=function () {
        $scope.typeShow=$ionicPopup.alert({
            template:` <button class="button button-block button-outline button-balanced" ng-click="meetingTypeHide('普通')">普通</button>
            <button class="button button-block button-outline button-balanced" ng-click="meetingTypeHide('视频')">视频</button>
            <button class="button button-block button-outline button-balanced" ng-click="meetingTypeHide('音频')">音频</button>`,
            title:'选择会议类型',
            scope:$scope,
            buttons:[]
        })
    };
    $scope.meetingTypeHide=function (msg) {
        $scope.typeShow.close();
        $scope.meetingType=msg;
    };

    //会议室信息

    // modal
    $ionicModal.fromTemplateUrl('templates/meetingRoom.html',{
        scope:$scope
    }).then(function (modal) {
        $scope.meetingModal=modal;
    });

    $scope.meetingRoomInfo=function () {

        WorkService.getHttp(`${$scope.serversIp}/work_weixin_webapp/data/OpenApiSample/example/meetingRoomList.php`,(suc)=>{
            console.log(suc);
            $scope.location=suc;
            $scope.roomList1=suc;
            $scope.meetingModal.show();
        },(err)=>{
            $ionicPopup.alert({
                title:'加载失败，请重试'
            })
        });

        $scope.mySlide=0;

        $scope.meetingRoom1=function (n,id) {
            if($scope.roomList1[n].children){
                $scope.roomList2=$scope.roomList1[n].children;
                $scope.mySlide=1;
            }else {
                $scope.requestRoom(id);
            }

        };

        $scope.meetingRoom2=function (n,id) {
            if($scope.roomList2[n].children){
                $scope.roomList3=$scope.roomList2[n].children;
                $scope.mySlide=2;
            }else {
                $scope.requestRoom(id)
            }
        };

        $scope.meetingRoom3=function (id) {
            $scope.requestRoom(id)
        };

        // 会议室滑动
        $scope.slideChange=function (index) {
            $scope.mySlide=index;
        };

        //请求会议室
        $scope.requestRoom=function (id) {
            WorkService.getHttp(`${$scope.serversIp}/work_weixin_webapp/data/OpenApiSample/example/locationIdRoomList.php?id=${id}`,(suc)=>{
                console.log(suc);
                if(!suc.length){
                    $ionicPopup.alert({
                        title:'没有会议室'
                    }).then(function () {
                        $scope.mySlide=0;
                        $scope.roomList1=$scope.location;
                    })
                }else {
                    $scope.roomList4=suc;
                    $scope.mySlide=3;
                }
            },(err)=>{
                console.log(err);
            })
        };

        //用户选择会议室
        $scope.selectRoom=function (id) {
            $scope.selectRoomId='room'+id;
            $scope.meetingModal.hide();
        }
    };

    //参会人员信息

    //modal
    $ionicModal.fromTemplateUrl('templates/peopleList.html',{
        scope:$scope
    }).then(function (modal) {
        $scope.peopleModal=modal;
    });

    //确认选择人员
    $scope.checkedUser=[];

    $scope.peopleInfo=function () {
        //请求部门列表
        WorkService.getHttp(`${$scope.serversIp}/work_weixin_webapp/data/OpenApiSample/example/department.php`,(suc)=>{
            // console.log(suc);
            if(suc.department.length){
                $scope.departList1=suc.department;
                $scope.peopleModal.show();
            }else {
                $ionicPopup.alert({
                    title:'加载失败，请重试'
                })
            }
        },(err)=>{
            $ionicPopup.alert({
                title:'加载失败，请重试'
            })
        });

        // 部门列表模板
        $ionicModal.fromTemplateUrl('templates/peopleList1.html',{
            scope:$scope
        }).then(function (modal) {
            $scope.usersModal=modal;
        });

        //id请求人员列表
        $scope.userList=[];

        //获得部门人员列表
        $scope.getUsers=function (id) {
            WorkService.getHttp(`${$scope.serversIp}/work_weixin_webapp/data/OpenApiSample/example/user_department.php?id=${id}`,(suc)=>{
                if(suc.userlist.length){
                    for (var i=0;i<suc.userlist.length;i++){
                        $scope.userList.push({
                            name:`${suc.userlist[i].name}(${suc.userlist[i].userid})`,
                            checked:false
                        })
                    }
                    $scope.usersModal.show();
                }else {
                    $ionicPopup.alert({
                        title:'加载失败，请重试'
                    })
                }
            },(err)=>{
                $ionicPopup.alert({
                    title:'加载失败，请重试'
                })
            });
            //全选
            $scope.allChecked={checked:false};
            $scope.toggleAllBox=function () {
                if($scope.allChecked.checked){
                    for (var i=0;i<$scope.userList.length;i++){
                        $scope.userList[i].checked=true;
                    }
                }else {
                    for (var i=0;i<$scope.userList.length;i++){
                        $scope.userList[i].checked=false
                    }
                }
            };

            //确认选择人员
            $scope.affirmChecked=function () {
                $scope.usersModal.hide();
                $scope.peopleModal.hide();
                for(var j=0;j<$scope.userList.length;j++){
                    ($scope.userList[j].checked==true)&&($scope.checkedUser.push($scope.userList[j].name));
                }
                if(!$scope.checkedUser.length){
                    $ionicPopup.alert({
                        title:"没有选择人员"
                    })
                }
            };
            $scope.deleteAllUser=function () {
                $scope.checkedUser=[];
            }
        }
    };


    //确认提交预约信息
    $scope.affirmOrderInfo=function () {
        $ionicPopup.alert({
            title:"预约成功"
        })
    }
}]);