/**
 * Created by king-bc on 2017/5/8.
 */

var app=angular.module('workApp',['ionic','ionic-datepicker']);

app.config(function ($stateProvider, $urlRouterProvider,$ionicConfigProvider,ionicDatePickerProvider) {
    $ionicConfigProvider.tabs.position('bottom');

    var datePickerObj={
        // inputDate: new Date(),
        // titleLabel: 'Select a Date',
        // setLabel: 'Set',
        // todayLabel: 'Today',
        // closeLabel: 'Close',
        // mondayFirst: false,
        // weeksList: ["S", "M", "T", "W", "T", "F", "S"],
        // monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
        inputDate: new Date(),
        titleLabel: '选择日期',
        setLabel: '设置',
        todayLabel: '今天',
        closeLabel: '关闭',
        mondayFirst: true,
        weeksList: [ "日","一", "二", "三", "四", "五", "六"],
        monthsList: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        templateType: 'popup',//modal or popup
        from: new Date(2012, 8, 1),
        to: new Date(2100, 8, 1),
        showTodayButton: true,
        dateFormat: 'dd MMMM yyyy',
        closeOnSelect: true,
        disableWeekdays: []
    };
    ionicDatePickerProvider.configDatePicker(datePickerObj);

    $stateProvider
       .state('home',{
           url:'/workHome',
           templateUrl:'tpl/home.html',
           controller:'homeCtrl'
       })
       .state('order',{
           url:'/workOrder',
           templateUrl:'tpl/order.html',
           controller:'orderCtrl'
       });
    $urlRouterProvider.otherwise('/workHome');
});

