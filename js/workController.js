/**
 * Created by king-bc on 2017/5/18.
 */
angular.module('workApp').controller('workCtrl',['$scope','$state','WorkService','$ionicPopup',function ($scope,$state,WorkService,$ionicPopup) {
    $scope.jump=function (state) {
        $state.go(state);
    };

    // 更新应用
    // WorkService.getHttp('http://172.16.10.81/work_weixin_webapp/data/OpenApiSample/example/app_manage.php?cmd=update',(suc)=>{
    //     console.log(suc)
    // },(err)=>{
    //     console.log(err)
    // });

    // WorkService.getHttp('http://172.16.10.81/work_weixin_webapp/data/OpenApiSample/example/jsapi.php',(suc)=>{
    //     $('#signPackage').html(suc);
    // });


    // 获取跳转链接内的code
    var url=window.location.href;
    var code=url.slice(url.indexOf('=')+1,url.indexOf('&'));
    $scope.serversIp='http://172.16.10.81';
    // 获取userId
    WorkService.getHttp(`${$scope.serversIp}/work_weixin_webapp/data/OpenApiSample/example/getUserInfo.php?code=${code}`,(suc)=>{
        $scope.userName=suc.UserId;
        localStorage.setItem('userName',suc.UserId);
    },(err)=>{
        alert(err);
    });

    $scope.openCamera=function () {
        $scope.cameraAlert=$ionicPopup.alert({
            title:'扫描二维码',
            template:`<button class="button button-block button-outline button-balanced" ng-click="affirmType(1)">签到</button>
                      <button class="button button-block button-outline button-balanced" ng-click="affirmType(2)">预约</button>
                      <button class="button button-block button-outline button-balanced" ng-click="affirmType(3)">延长会议</button>
                      <button class="button button-block button-outline button-balanced" ng-click="affirmType(4)">结束会议</button>`,
            scope:$scope,
            buttons:[]
        })
    };

    //确认扫码类型
    $scope.affirmType=function (msg) {
        jWeixin.scanQRCode({
            needResult:1,
            desc:"scanQRCode desc",
            success:function (a) {
                $ionicPopup.alert({
                    title:JSON.stringify(a)
                })
            },
            fail:function (a) {
                alert(JSON.stringify(a))
            }
        });
        $scope.cameraAlert.close();
    }

}]);