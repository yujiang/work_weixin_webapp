<?php
    /**
     * 部门ID查询部门人员
     */
    require_once "../lib/txl_api.php";

    $api=new TXL_API();

    $id = isset($_GET["id"]) ? $_GET["id"] : 1;

    print ($api->queryUsersByDepartmentId($id));
