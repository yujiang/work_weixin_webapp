    var shareConfig={
        title:"\u4e92\u8054\u7f51\u4e4b\u5b50",
        desc:"\u5728\u957f\u5927\u7684\u8fc7\u7a0b\u4e2d\uff0c\u6211\u624d\u6162\u6162\u53d1\u73b0\uff0c\u6211\u8eab\u8fb9\u7684\u6240\u6709\u4e8b\uff0c\u522b\u4eba\u8ddf\u6211\u8bf4\u7684\u6240\u6709\u4e8b\uff0c\u90a3\u4e9b\u6240\u8c13\u672c\u6765\u5982\u6b64\uff0c\u6ce8\u5b9a\u5982\u6b64\u7684\u4e8b\uff0c\u5b83\u4eec\u5176\u5b9e\u6ca1\u6709\u975e\u5f97\u5982\u6b64\uff0c\u4e8b\u60c5\u662f\u53ef\u4ee5\u6539\u53d8\u7684\u3002\u66f4\u91cd\u8981\u7684\u662f\uff0c\u6709\u4e9b\u4e8b\u65e2\u7136\u9519\u4e86\uff0c\u90a3\u5c31\u8be5\u505a\u51fa\u6539\u53d8\u3002",
        link:"http://movie.douban.com/subject/25785114/",
        imgUrl:"http://demo.open.weixin.qq.com/jssdk/images/p2166127561.jpg",
        success:function(a){alert("\u5df2\u5206\u4eab")},
        cancel:function(a){alert("\u5df2\u53d6\u6d88")},
        fail:function(a){alert(JSON.stringify(a))}
    },

    images={localId:[],serverId:[]},

    voice={localId:"",serverId:""};

    wx.ready(function(){
        $("[data-type]").on("click",function(a){
            function b(){
            wx.uploadImage({
                localId:images.localId[f],
                success:function(a){
                    f++,images.serverId.push(a.serverId),g>f&&b()
                },
                fail:function(a){
                    alert(JSON.stringify(a))
                }
            })
        }

            function c(){
            wx.downloadImage({
                serverId:images.serverId[f],
                success:function(a){
                    f++,alert("\u5df2\u4e0b\u8f7d\uff1a"+f+"/"+g),images.localId.push(a.localId),g>f&&c()
                }
            })
        }

            var d=$(a.target),e=d.attr("data-type");

            switch(e){
            case"checkJsApi":wx.checkJsApi({
                jsApiList:["getNetworkType","previewImage"],
                success:function(a){alert(JSON.stringify(a))}});break;
            case"onMenuShareAppMessage":wx.onMenuShareAppMessage(shareConfig),alert("\u5df2\u6ce8\u518c\u83b7\u53d6\u201c\u8f6c\u53d1\u7ed9\u540c\u4e8b\u201d\u72b6\u6001\u4e8b\u4ef6");break;
            case"onMenuShareWechat":wx.onMenuShareWechat(shareConfig),alert("\u5df2\u6ce8\u518c\u83b7\u53d6\u201c\u5fae\u4fe1\u5206\u4eab\u7ed9\u670b\u53cb\u201d\u72b6\u6001\u4e8b\u4ef6");break;
            case"chooseImage":wx.chooseImage({
                count:1,
                sizeType:["original","compressed"],
                sourceType:["album","camera"],
                success:function(a){
                    images.localId=a.localIds,alert("\u5df2\u9009\u62e9 "+a.localIds.length+" \u5f20\u56fe\u7247")
                }
            });break;
            case"previewImage":wx.previewImage({
                current:"http://img3.douban.com/view/photo/photo/public/p2152117150.jpg",
                urls:["http://img3.douban.com/view/photo/photo/public/p2152117150.jpg","http://img3.douban.com/view/photo/photo/public/p2152134700.jpg"]});break;
            case"uploadImage":if(0==images.localId.length)return void alert("\u8bf7\u5148\u4f7f\u7528 chooseImage \u63a5\u53e3\u9009\u62e9\u56fe\u7247");var f=0,g=images.localId.length;images.serverId=[],b();break;
            case"downloadImage":if(0===images.serverId.length)return void alert("\u8bf7\u5148\u4f7f\u7528 uploadImage \u4e0a\u4f20\u56fe\u7247");var f=0,g=images.serverId.length;images.localId=[],c();break;
            case"startRecord":wx.startRecord({
                cancel:function(){
                    alert("\u7528\u6237\u62d2\u7edd\u6388\u6743\u5f55\u97f3")
                }
            });break;
            case"stopRecord":wx.stopRecord({
                success:function(a){voice.localId=a.localId},
                fail:function(a){alert(JSON.stringify(a))}});break;
            case"playVoice":if(""==voice.localId)return void alert("\u8bf7\u5148\u4f7f\u7528 startRecord \u63a5\u53e3\u5f55\u5236\u4e00\u6bb5\u58f0\u97f3");wx.playVoice({localId:voice.localId});break;
            case"pauseVoice":wx.pauseVoice({localId:voice.localId});break;
            case"stopVoice":wx.stopVoice({localId:voice.localId});break;
            case"uploadVoice":if(""==voice.localId)return void alert("\u8bf7\u5148\u4f7f\u7528 startRecord \u63a5\u53e3\u5f55\u5236\u4e00\u6bb5\u58f0\u97f3");wx.uploadVoice({
                localId:voice.localId,
                success:function(a){
                    alert("\u4e0a\u4f20\u8bed\u97f3\u6210\u529f\uff0cserverId \u4e3a"+a.serverId),voice.serverId=a.serverId
                }
            });break;
            case"downloadVoice":if(""==voice.serverId)return void alert("\u8bf7\u5148\u4f7f\u7528 uploadVoice \u4e0a\u4f20\u58f0\u97f3");
            wx.downloadVoice({
                serverId:voice.serverId,
                success:function(a){
                    alert("\u4e0b\u8f7d\u8bed\u97f3\u6210\u529f\uff0clocalId \u4e3a"+a.localId),voice.localId=a.localId
                }
            });break;
            case"getNetworkType":wx.getNetworkType({
                success:function(a){alert(a.networkType)},
                fail:function(a){alert(JSON.stringify(a))
                }
            });break;
            case"openLocation":wx.openLocation({
                latitude:23.099994,
                longitude:113.32452,
                name:"TIT \u521b\u610f\u56ed",
                address:"\u5e7f\u5dde\u5e02\u6d77\u73e0\u533a\u65b0\u6e2f\u4e2d\u8def 397 \u53f7",
                scale:14,
                infoUrl:"http://weixin.qq.com"
            });break;
            case"getLocation":wx.getLocation({
                success:function(a){alert(JSON.stringify(a))},
                cancel:function(a){
                    alert("\u7528\u6237\u62d2\u7edd\u6388\u6743\u83b7\u53d6\u5730\u7406\u4f4d\u7f6e")
                }
            });break;
            case"hideOptionMenu":wx.hideOptionMenu();break;
            case"showOptionMenu":wx.showOptionMenu();break;
            case"closeWindow":wx.closeWindow();break;
            case"hideMenuItems":wx.hideMenuItems({
                menuList:["menuItem:share:appMessage","menuItem:share:wechat","menuItem:favorite"],
                success:function(a){
                    alert("\u5df2\u9690\u85cf\u201c\u8f6c\u53d1\u201d\uff0c\u201c\u5fae\u4fe1\u201d\uff0c\u201c\u6536\u85cf\u201d\u6309\u94ae")
                },
                fail:function(a){
                    alert(JSON.stringify(a))
                }
            });break;
            case"showMenuItems":wx.showMenuItems({
                menuList:["menuItem:share:appMessage","menuItem:share:wechat","menuItem:favorite"]
            });break;
            case"hideAllNonBaseMenuItem":wx.hideAllNonBaseMenuItem({
                success:function(){
                    alert("\u5df2\u9690\u85cf\u6240\u6709\u975e\u57fa\u672c\u83dc\u5355\u9879")
                }
            });break;
            case"showAllNonBaseMenuItem":wx.showAllNonBaseMenuItem({
                success:function(){
                    alert("\u5df2\u663e\u793a\u6240\u6709\u975e\u57fa\u672c\u83dc\u5355\u9879")
                }
            });break;
            case"scanQRCode0":wx.scanQRCode();break;
            case"scanQRCode1":wx.scanQRCode({
                needResult:1,desc:"scanQRCode desc",
                success:function(a){alert(JSON.stringify(a))
                }
            })
        }
        });

        var a={
            title:"\u4f01\u4e1a\u5fae\u4fe1JS-SDK Demo",
            desc:"\u4f01\u4e1a\u5fae\u4fe1JS-SDK,\u5e2e\u52a9\u4f01\u4e1a\u63d0\u4f9b\u6700\u4f18\u8d28\u7684\u670d\u52a1",
            link:"http://open.work.weixin.qq.com/wwopen/jsapidemo",
            imgUrl:"http://mmbiz.qpic.cn/mmbiz/icTdbqWNOwNRt8Qia4lv7k3M9J1SKqKCImxJCt7j9rHYicKDI45jRPBxdzdyREWnk0ia0N5TMnMfth7SdxtzMvVgXg/0"
        };

        wx.onMenuShareAppMessage(a);
        wx.onMenuShareTimeline(a)
    });

    wx.error(function(a){alert(a.errMsg)});